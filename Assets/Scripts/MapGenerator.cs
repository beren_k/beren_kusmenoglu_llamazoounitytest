﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), (typeof(MeshCollider)), (typeof(MeshRenderer)))]
public class MapGenerator : MonoBehaviour {

    public CellMap cellMap;
    public int mapHeight = 0;
    public int mapWidth = 0;
    public bool useSeed = true;
    public bool drawMesh = true;

    public int caveNo = 0;
    private void OnAwake()
    {
        drawMesh = false;
       
    }

    public void GenerateMap()
    {
       
        if (cellMap == null)
            cellMap = new CellMap(mapWidth, mapHeight, useSeed);

        cellMap.Init();
        AddBorders();
      
        if (drawMesh)
        {
            MeshGenerator meshGenerator = GetComponent<MeshGenerator>();
            meshGenerator.GenerateMesh(cellMap.Map, 1);
        }

       
    }

    public void AddBorders()
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if ((i <= 3 || j <= 3) || (i >= mapWidth - 3 || j >= mapHeight - 3))
                    cellMap.Map[i, j] = 1;
            }
        }
    }
    public void SpawnBalls()
    {
        GetComponent<MeshGenerator>().SpawnBalls();
    }

    public void ClearMap()
    {

        cellMap = null;
        
        MeshGenerator meshGenerator = GetComponent<MeshGenerator>();
        meshGenerator.ClearMesh();

        GameObject[] balls = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject ball in balls)
        {
            DestroyImmediate(ball);
        }

    }

    public void ConnectCaves()
    {
        if (cellMap != null)
            GetComponent<IslandLocator>().FindEmptyCell();

    }

    private void OnValidate()
    {
  
        cellMap = null;
        GenerateMap();

        if (drawMesh)
        {
            MeshGenerator meshGenerator = GetComponent<MeshGenerator>();
            meshGenerator.GenerateMesh(cellMap.Map, 1);
        }
    }


    public void CreatePrefabFromMap()
    {
        if (GetComponent<MeshFilter>().sharedMesh == null)
        {
            Debug.LogError("No mesh created! Cant save prefab!");
            return;
        }
       
        string localPath = "Assets/Resources/Caves/" + "Cave_" + caveNo +  ".prefab";

        GameObject prefab = gameObject;

        /**
        prefab.AddComponent<MeshFilter>();
        prefab.GetComponent<MeshFilter>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;

        prefab.AddComponent<MeshRenderer>();
        // prefab.GetComponent<MeshRenderer>().sharedMaterial = GetComponent<MeshRenderer>().mesh;

        prefab.AddComponent<MeshCollider>();
        prefab.GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshCollider>().sharedMesh;
        
        Instantiate(GetComponent<MeshGenerator>().walls).transform.parent = prefab.transform;
   
        //Object temp = PrefabUtility.CreateEmptyPrefab(localPath);

        //PrefabUtility.ReplacePrefab(gameObject, temp, ReplacePrefabOptions.ConnectToPrefab);
        */
        PrefabUtility.CreatePrefab(localPath, prefab);

        caveNo++;
    }


    void OnDrawGizmos()
    {
        
        if((cellMap != null) && !drawMesh)
        {
           
            for (int i = 0; i < cellMap.Width; i++)
            {
                for (int j = 0; j < cellMap.Height; j++)
                {
                    switch (cellMap.Map[i,j])
                    {
                        case 0:
                            Gizmos.color = Color.white;
                            break;
                        case 1:
                            Gizmos.color = Color.black;
                            break;
                        case 2:
                            Gizmos.color = Color.red;
                            break;
                        case 3:
                            Gizmos.color = Color.green;
                            break;
                             
                        case 4:
                            Gizmos.color = Color.yellow;
                            break;
                        case 5:
                            Gizmos.color = Color.magenta;
                            break; 
                          
                        case 6:
                            Gizmos.color = Color.cyan;
                            break;
                           
                   
                        default:
                            Gizmos.color = Color.white;
                            break;
                    }
                  
                    Vector3 pos = new Vector3(-mapWidth / 2 + i + 0.5f, 0, -mapHeight / 2 + j + 0.5f);
                    Gizmos.DrawCube(pos, Vector3.one);
                   
                }
            }
        }

         
      
    }

}
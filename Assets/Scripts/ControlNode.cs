﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlNode : Node {
    public bool active;
    public Node above, right;

    public ControlNode (Vector3 pos, bool active, float squareSize) : base(pos)
    {
        this.active = active;
        this.above = new Node(pos + Vector3.forward * squareSize / 2.0f);
        this.right = new Node(pos + Vector3.right * squareSize / 2.0f);
    }

}

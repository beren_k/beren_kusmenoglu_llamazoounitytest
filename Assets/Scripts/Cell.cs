﻿using System.Collections;
using System.Collections.Generic;

public class Cell
{
    public int x;
    public int y;
    public bool lookedAt;

    public Cell(int x, int y)
    {
        this.x = x;
        this.y = y;
        this.lookedAt = false;

    }

    public override string ToString()
    {
        return "[ " + x + ", " + y + " ]";
    }

    public override bool Equals(object obj)
    {
        Cell cell = (Cell)obj;
        return (cell.x == this.x && cell.y == this.y);
    }

};
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandLocator : MonoBehaviour
{

    private int[,] map;

    private List<Cell> emptyCells = new List<Cell>();
    private List<Cell> checkedCells = new List<Cell>();

    private List<List<Cell>> islands = new List<List<Cell>>();

    private int islandIndex = 0;

    public void FindEmptyCell()
    {
        islands = new List<List<Cell>>();
        emptyCells.Clear();
        checkedCells.Clear();
        islands.Clear();
        islandIndex = 0;

        if (GetComponent<MapGenerator>().cellMap != null)
        {

            map = GetComponent<MapGenerator>().cellMap.Map;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == 0)
                    {
                        //map[i, j] = 2;
                        emptyCells.Add(new Cell(i, j));

                    }
                }
            }
        }
        FindIslandNeighbours();
        GetComponent<MeshGenerator>().GenerateMesh(map, 1);
    }

    public void ColorIslands()
    {
        for (int i = 0; i < islands.Count; i++)
        {
            foreach (Cell coord in islands[i])
            {
                map[coord.x, coord.y] = i + 2;
                //print(coord.ToString() + " with island id:  " + i);
            }

        }
    }

    public void FillSmallIslands()
    {
        for (int i = 0; i < islands.Count; i++)
        {

            if (islands[i].Count <= (map.GetLength(0) * map.GetLength(1) * 0.005f))
            {

                foreach (Cell coord in islands[i])
                {
                    map[coord.x, coord.y] = 1;

                }
                islandIndex--;
                islands.RemoveAt(i);
            }

        }
    }


    public void FindIslandNeighbours()
    {
        foreach (Cell coords in emptyCells)
        {
            if (!checkedCells.Contains(coords))
            {

                if (coords.x > 0 && coords.y > 0 && coords.x < map.GetLength(0) && coords.y < map.GetLength(1))
                    FloodFill(coords);

                islandIndex++;
            }

            //map[coords.x, coords.y] = 2;
        }
        ColorIslands();
        FillSmallIslands();

        print(islands.Count - 1 + " islands found");
        ConnectIslands();
    }

    public void ConnectIslands()
    {
        for (int i = 0; i < islands.Count - 1; i++)
        {
            System.Random rnd = new System.Random();

            int rndCellIndex = rnd.Next(islands[i].Count);

            Cell start = islands[i][rndCellIndex];

            // if (i <= islands.Count)
            {
                rndCellIndex = rnd.Next(islands[i + 1].Count);

                Cell end = islands[i + 1][rndCellIndex];

                ConnectPoints(start, end);

                //print("Start: " + map[start.x, start.y]);
                map[start.x, start.y] = 99;
                map[end.x, end.y] = 100;
                //print("End: " + map[end.x, end.y]);
            }


            /*
            else if (islands[i - 1] != null)
            {
                rndCellIndex = rnd.Next(islands[i - 1].Count);

                Cell end = islands[i - 1][rndCellIndex];

                ConnectPoints(start, end);
            }*/

        }
    }

    void ConnectPoints(Cell p0, Cell p1)
    {
        int startingIsland = map[p0.x, p0.y];

        if ((p0.x >= 0 && p0.y >= 0 && p0.x <= map.GetLength(0) && p0.y <= map.GetLength(1)) && (p1.x >= 0 && p1.y >= 0 && p1.x <= map.GetLength(0) && p1.y <= map.GetLength(1)))
        {
            int x = p0.x;
            int x2 = p1.x;
            int y = p0.y;
            int y2 = p1.y;

            int w = x2 - x;
            int h = y2 - y;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = Mathf.Abs(w);
            int shortest = Mathf.Abs(h);
            if (!(longest > shortest))
            {
                longest = Mathf.Abs(h);
                shortest = Mathf.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;

            for (int i = 0; i <= longest; i++)
            {
                //putpixel(x, y, color);

                for (int a = x - 1; a < x + 1; a++)
                {
                    for (int b = y - 1; b < y + 1; b++)
                    {
                        if ((a >= 0 && b >= 0 && a <= map.GetLength(0) && b <= map.GetLength(1)))
                        {
                            int val = map[a, b];
                            if (val == 1)
                                map[a, b] = 89;
                            if (val > 1 && val != startingIsland)
                                break;
                            //    if (val != 1 && val != startingIsland && val!= 89)
                            //        return;

                        }
                    }

                }

                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x += dx1;
                    y += dy1;
                }
                else
                {
                    x += dx2;
                    y += dy2;
                }
            }


        }


    }



    public void FloodFill(Cell coords)
    {
        if (islands == null)
            islands = new List<List<Cell>>();


        if (islands.Count - 1 < islandIndex)
            islands.Add(new List<Cell>());


        int x = coords.x;
        int y = coords.y;

        Cell N = new Cell(x, y - 1);
        //Cell NE = new Cell(x + 1, y - 1);
        Cell E = new Cell(x + 1, y);
        //Cell SE = new Cell(x + 1, y + 1);
        Cell S = new Cell(x, y + 1);
        //Cell SW = new Cell(x - 1, y + 1);
        Cell W = new Cell(x - 1, y);
        //Cell NW = new Cell(x - 1, y - 1);

        //List<Cell> neighbours = new List<Cell>{ N, NE, E, SE, S, SW, W, NW };
        List<Cell> neighbours = new List<Cell> { N, E, S, W };

        foreach (Cell cell in neighbours)
        {
            if (map[cell.x, cell.y] == 0 && !checkedCells.Contains(cell))
            {
                //map[cell.x, cell.y] = islandIndex;
                islands[islandIndex].Add(cell);
                checkedCells.Add(cell);
                FloodFill(cell);
            }
        }

        //original
        //map[coords.x, coords.y] = islandIndex;
        islands[islandIndex].Add(coords);
        checkedCells.Add(coords);

    }

}

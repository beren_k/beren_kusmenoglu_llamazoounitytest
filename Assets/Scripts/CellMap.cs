﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CellMap
{
    private int width;
    private int height;
    private bool usesSeed;
    private int seed;
    private int[,] map;

    public CellMap()
    {
        this.Width = 10;
        this.Height = 10;
        this.UsesSeed = true;
        this.Seed = GenerateSeed();
        this.Map = GenerateMap();
        FillMap();
        ExpandCells(5);
    }

    public CellMap(int width, int height, bool usesSeed)
    {
        this.Width = width;
        this.Height = height;
        this.UsesSeed = usesSeed;
        this.Seed = GenerateSeed();
        this.Map = GenerateMap();
        FillMap();
        ExpandCells(5);
    }

    // Generates and returns seed if it is enabled
    int GenerateSeed()
    {
        if (this.UsesSeed)
            return (Time.realtimeSinceStartup.ToString()).GetHashCode();

        return 1;
    }

    int[,] GenerateMap()
    {
        return new int[this.Width, this.Height];
    }

    void FillMap()
    {
        System.Random rnd = new System.Random(this.Seed);
   
        for (int i = 0; i < this.Width; i++)
        {
            for (int j = 0; j < this.Height; j++)
            {
                if (i == 0 || j == 0 || i == this.Width - 1 || j == this.height - 1)
                    this.Map[i, j] = 1;
                else
                   this.Map[i, j] = rnd.Next(0, 2);
            }
        }
    }

    void ExpandCells(int iteration)
    {
        for (int it = 0; it < iteration; it++)
        { 
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    int numberOfWalls = GetSurroundingWalls(i, j);
                   // Debug.Log(numberOfWalls);
                    if (numberOfWalls > 4)
                        this.Map[i, j] = 1;
                    else if (numberOfWalls < 4)
                        this.Map[i, j] = 0;

                }
            }
        }
    }

    int GetSurroundingWalls(int posX, int posY)
    {
        int numberOfWalls = 0;
        // Looking for the cells in a 3x3 grid.
        for (int i = posX - 1; i <= posX + 1; i++)
        {
            for (int j = posY - 1; j <= posY + 1; j++)
            {
                // if we are not looking at the given cell
                if (i != posX || j != posY)
                {
                    // bounds checking
                    if (i < width && i >= 0 && j < height && j >= 0)
                        numberOfWalls += this.Map[i, j];
                    else
                        numberOfWalls++;

                }
               
            }
        }
        return numberOfWalls;
    }

    public void Init()
    {
        this.Seed = GenerateSeed();
        FillMap();
        ExpandCells(5);
    }

    public void ClearMap()
    {
        this.map = new int[width, height];
    }

    public override string ToString()
    {
        return base.ToString();
    }


    // Encapsulated fields

    public int Width
    {
        get
        {
            return width;
        }

        set
        {
            width = value;
        }
    }

    public int Height
    {
        get
        {
            return height;
        }

        set
        {
            height = value;
        }
    }

    public bool UsesSeed
    {
        get
        {
            return usesSeed;
        }

        set
        {
            usesSeed = value;
        }
    }

    public int Seed
    {
        get
        {
            return seed;
        }

        set
        {
            seed = value;
        }
    }

    public int[,] Map
    {
        get
        {
            return map;
        }

        set
        {
            map = value;
        }
    }


}
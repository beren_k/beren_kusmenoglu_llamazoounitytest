﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
public class GenerationEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        MapGenerator mg = (MapGenerator)target;

      
        EditorGUILayout.LabelField("Generation", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Generate Map"))
        {
            mg.GenerateMap();


        }

        if (GUILayout.Button("Connect Caves"))
        {
            mg.ConnectCaves();


        }
        if (GUILayout.Button("Clear Map"))
        {
            mg.ClearMap();
        }

        if (GUILayout.Button("Spawn Balls"))
        {
            mg.SpawnBalls();
        }

       
        GUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Saving", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save cave"))
        {
            mg.CreatePrefabFromMap();
        }
        if (GUILayout.Button("Reset cave number"))
        {
            mg.caveNo = 0;
        }
        GUILayout.EndHorizontal();


    }

}
